/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robot_project;

/**
 *
 * @author nymr3kt
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(2,1,5,6,100);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('N',9);
        System.out.println(robot);
        robot.walk(1);
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk('W', 5);
        System.out.println(robot);
        robot.walk('S',6);
        robot.walk('E',5);
        System.out.println(robot);
    }
}
