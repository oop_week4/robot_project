/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robot_project;

/**
 *
 * @author nymr3kt
 */
public class Robot {
    private int N;
    private int x;
    private int y;
    private int xb;
    private int yb;
    private char lastdirection ='N';
    
    public Robot(int x, int y , int xb ,int yb , int N){
        this.x = x ;
        this.y = y ;
        this.xb = xb ;
        this.yb = yb ;
        this.N = N ;
        
    }
    public boolean inMap(int x , int y){
        if(x >= N || x < 0 || y >= N || y < 0 ){
            return false ;
        }
        return true ;
    }
    
    public boolean walk(char direction){
        switch (direction){
            case 'N' :
                if(!inMap(x, y + 1)){
                    printUnlable();
                    return false ;
                }
                y = y + 1;
                break;
            case 'S' :
                if(!inMap(x, y - 1)){
                    printUnlable();
                    return false ;
                }
                y = y - 1;
                break;
            case 'E' :
                if(!inMap(x + 1 , y )){
                    printUnlable();
                    return false ;
                }
                x = x + 1;
                break;
            case 'W' :
                if(!inMap(x - 1, y )){
                    printUnlable();
                    return false ;
                }
                x = x - 1 ;
                break;      
        }
        
        lastdirection = direction ;
        if(isBomb()){
            System.out.println("Bomb Found!!!");
        }
        return true ;
        
    }
    public boolean walk(char direction ,int step) {
        for(int i = 0 ; i < step ; i++){
            if(!walk(direction)){
                return false ;
            }
        }
        return true ;
    }
    
    public boolean walk(){
        return walk(lastdirection) ;
    }
    public boolean walk(int step){
        return walk(lastdirection, step) ;
    }
    
    public void printUnlable(){
        System.out.println("I can't move!!!");
    }
    
    public String toString(){
        return "Robot (" + this.x + " , " + this.y + ")" ;
        
    }
    public boolean isBomb(){
        if(x == xb && y == yb){
            return true ;
        }
        return false ;
    }
    
}
